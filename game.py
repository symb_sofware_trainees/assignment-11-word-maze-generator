import random
class Word_maze:
    def __init__(self, size):
        self.size = size
        self.grid = [[' ' for _ in range(size)] for _ in range(size)]
        
    def place_word_in_game(self, word):
        word=random.choice([word,word[::-1]])
        direction = random.choice([[1,0], [0,1], [1,1]])
        row, col = self.get_start_point(word,direction)
        if row == -1:
            return False
        for i, character in enumerate(word):
            if self.grid[row+direction[0]*i][col+direction[1]*i] != ' ' and self.grid[row+direction[0]*i][col+direction[1]*i] != character:
                return False
        for i, character in enumerate(word):
            self.grid[row+direction[0]*i][col+direction[1]*i] = character
        print(word)
        self.print_game()
        return True

    def get_start_point(self, word, direction):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    if i + len(word) <= self.size and j + len(word) <= self.size:
                        if all([self.grid[i+direction[0]*k][j+direction[1]*k] == ' ' or self.grid[i+direction[0]*k][j+direction[1]*k] == word[k] for k in range(len(word))]):
                            return i, j
        return -1, -1

    def fill_random_characters(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    self.grid[i][j] = random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
# for debugging use 'abcdefghijklmnopqrstuvwxyz'
    def print_game(self):
        for row in self.grid:
            print(' '.join(row))

if __name__ == '__main__':
    words =["mercury", "venus", "earth", "jupiter", "mars", "saturn", "uranus", "neptune", "pluto"]
    word_maze = Word_maze(len(max(words,key=len))+5)
    for word in words:
        word=word.upper()
        while not word_maze.place_word_in_game(word):
            pass
    word_maze.fill_random_characters()
    word_maze.print_game()